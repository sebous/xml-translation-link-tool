import fs from 'fs';
import path from 'path';
import { promisify } from 'util';
import { parseStringPromise, Builder } from 'xml2js';
const readFile = (fileName: string) => promisify(fs.readFile)(fileName, 'utf8');
const writeFile = (fileName: string, data: string) => promisify(fs.writeFile)(fileName, data, 'utf8');

(async () => {
    const data: string = await readFile(path.join(__dirname, './data.xml'));
    const xml = await parseStringPromise(data);

    const linkElements: any[] = [];
    const sections: any[] = [];
    JSON.stringify(xml, (key, value) => {
        if (key === 'xref') {
            linkElements.push(value);
        }
        if (value?.[0]?.$?.['xml:id']) {
            value.forEach((v: any) => sections.push(v));
        }
        return value;
    });
    console.log(`links count: ${linkElements.reduce((count, linksArr) => count + linksArr.length, 0)}`);
    const sectionIds = sections.map(sec => sec?.$?.['xml:id']);
    const externalLinks = linkElements
        .flat()
        .map(link => link.$)
        .filter(link => sectionIds.indexOf(link.linkend) <= -1);
    console.log(`links external: ${externalLinks.length}`);

    writeFile(path.join(__dirname, './output.txt'), externalLinks.map(link => `${link.linkend}\n${link.xrefstyle}`).join('\n' + ' ' + '\n'));
})()
